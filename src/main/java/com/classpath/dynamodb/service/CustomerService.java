package com.classpath.dynamodb.service;

import org.springframework.stereotype.Service;

import com.classpath.dynamodb.model.Customer;
import com.classpath.dynamodb.repository.CustomerRepository;

@Service
public class CustomerService {
	
	private final CustomerRepository customerRepository;
	
	public CustomerService(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}
	
	public void save(Customer customer) {
		this.customerRepository.save(customer);
	}

}
