package com.classpath.dynamodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsDynamodbCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsDynamodbCrudApplication.class, args);
	}

}
