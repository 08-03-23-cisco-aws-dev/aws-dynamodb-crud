package com.classpath.dynamodb.config;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;

@Configuration
@EnableDynamoDBRepositories(basePackages = "com.classpath.dynamodb.repository")
public class AppConfig {
	
	@Bean
	public AmazonDynamoDB amazonDynamoDB() {
		ProfileCredentialsProvider credentialsProvider = new ProfileCredentialsProvider("dynamodb");
		AmazonDynamoDB amazonDynamoDB = AmazonDynamoDBClientBuilder.standard().withCredentials(credentialsProvider).build();
		return amazonDynamoDB;
	}

}
