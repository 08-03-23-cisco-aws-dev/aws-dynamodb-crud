package com.classpath.dynamodb.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.classpath.dynamodb.model.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, String>{

}
