package com.classpath.dynamodb.client;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.classpath.dynamodb.model.Customer;
import com.classpath.dynamodb.service.CustomerService;

@Component
public class Client implements CommandLineRunner{

	private final CustomerService customerService;
	
	public Client(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	@Override
	public void run(String... args) throws Exception {
	
		Customer customer = new Customer();
		customer.setCustomerID("12");
		customer.setEmail("raju@gmail.com");
		this.customerService.save(customer);
		
		
	}

}
